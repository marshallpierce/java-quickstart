package org.mpierce.example;

import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mpierce.example.StringTool.repeat;

public class StringToolTest {

    @Test
    public void testRepeat0() {
        assertEquals(0, repeat("foo", 0).count());
    }

    @Test
    public void testFailsWithNegativeRepeat() {
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
                () -> repeat("foo", -1));
        assertEquals("num must be nonnegative, was -1", e.getMessage());
    }

    @Test
    public void testRepeat1() {
        IntStream intStream = repeat("foo", 1);
        String result =
                getString(intStream);

        assertEquals("foo", result);
    }

    @Test
    public void testRepeatMany() {
        assertEquals("foofoofoo", getString(repeat("foo", 3)));
    }

    private static String getString(IntStream intStream) {
        return intStream
                .collect(StringBuilder::new,
                        (buf, i) -> buf.append((char) i),
                        StringBuilder::append)
                .toString();
    }
}
