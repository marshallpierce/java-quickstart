package org.mpierce.example;

import java.util.NoSuchElementException;
import java.util.PrimitiveIterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;
import javax.annotation.concurrent.NotThreadSafe;

/**
 * A faintly useful class just to be some example code that can have tests written.
 */
public final class StringTool {
    /**
     * Repeat a string a given number of times, expressed as a stream of chars (represented as an IntStream).
     *
     * @param s   the string to repeat
     * @param num how many times
     * @return s, concatenated num times
     */
    static IntStream repeat(String s, int num) {
        if (num < 0) {
            throw new IllegalArgumentException("num must be nonnegative, was " + num);
        }

        if (num == 0) {
            return IntStream.empty();
        }

        Spliterator.OfInt spliterator =
                Spliterators.spliterator(new StringRepeatCharIterator(s, num), (long) s.length() * num, 0);
        return StreamSupport
                .intStream(spliterator, false);
    }

    @NotThreadSafe
    static class StringRepeatCharIterator implements PrimitiveIterator.OfInt {

        private final String s;
        private final int num;
        private long posToEmitNextInStream;

        StringRepeatCharIterator(String s, int num) {
            this.s = s;
            this.num = num;
        }

        @Override
        public int nextInt() {

            if (!hasNext()) {
                throw new NoSuchElementException("Already consumed all characters");
            }

            int ch = s.charAt((int) (posToEmitNextInStream % s.length()));

            posToEmitNextInStream++;

            return ch;
        }

        @Override
        public boolean hasNext() {
            return posToEmitNextInStream < s.length() * num;
        }
    }
}
